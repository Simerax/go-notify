//go:build !unix && !windows

package notify

func newNotifier(appName string) (Notifier, error) {
	return nil, ErrPlatformNotSupported
}
