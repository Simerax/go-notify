package main

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/Simerax/go-notify"
)

func main() {

	n, err := notify.NewNotifier("my app")
	if err != nil {
		panic(err)
	}
	defer n.Close()

	actionHandler := make(chan (string), 1)

	go func() {
		for action := range actionHandler {
			fmt.Println("Handling Action:", action)
		}
		fmt.Println("Done with handling actions")
	}()

	notification := notify.Notification{
		Summary: "Hello from Go!",
		Body:    "A little more text...",
		Urgency: notify.UrgencyLow,
		Timeout: time.Second * 5,
		Actions: []notify.Action{
			notify.Action{"default", "default action"},
		},
		OnAction: actionHandler,
	}

	fmt.Println("Sending notification")
	if err := n.Notify(context.Background(), notification); err != nil {
		panic(err)
	}

	fmt.Println("waiting...")
	time.Sleep(time.Second * 6)
}
