//go:build windows

package notify

import (
	"context"
	"syscall"
	"time"
	"unsafe"

	"github.com/pkg/errors"
	"github.com/zzl/go-win32api/win32"
)

type notifier struct {
	appName string
	hwnd    win32.HWND
}

func newNotifier(appName string) (Notifier, error) {

	hwnd, err := createWindow(appName)
	if err != nil {
		return nil, err
	}

	return &notifier{
		appName: appName,
		hwnd:    hwnd,
	}, nil
}

func (n *notifier) Close() error {
	win32.DestroyWindow(n.hwnd)
	win32.UnregisterClass(win32.StrToPwstr(n.appName), 0)
	return nil
}

func (n *notifier) Notify(ctx context.Context, notification Notification) error {

	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
	}

	if len(notification.Actions) > 0 {
		return ErrActionsNotSupported
	}

	title, err := syscall.UTF16FromString(notification.Summary)
	if err != nil {
		return errors.Wrap(err, "failed to allocate title")
	}
	if len(title) > 128 {
		title = title[0:128]
	}

	body, err := syscall.UTF16FromString(notification.Body)
	if err != nil {
		return errors.Wrap(err, "failed to allocate body")
	}
	if len(body) > 256 {
		body = body[0:128]
	}

	nid := win32.NOTIFYICONDATAW{}
	nid.CbSize = (uint32)(unsafe.Sizeof(nid))
	nid.HWnd = n.hwnd
	nid.UID = 0
	nid.UFlags = win32.NIF_INFO

	if notification.IconPath != "" {
		nid.UFlags |= win32.NIF_ICON
		iconFlags := win32.LR_LOADFROMFILE | win32.LR_DEFAULTSIZE
		hIcon, errCode := win32.LoadImage(0, win32.StrToPwstr(notification.IconPath), win32.IMAGE_ICON, 0, 0, iconFlags)
		if errCode != 0 {
			return errors.Errorf("win32: LoadIcon failed: Code %d", errCode)
		}
		defer win32.DestroyIcon(hIcon)
		nid.HIcon = hIcon
	}

	// Data is a union and in case of NIF_INFO we can specify the timeout here
	// but timeout is only honored in windows 2000 and xp
	nid.Data[0] = uint32(notification.Timeout.Milliseconds())
	copy(nid.SzInfoTitle[:], title)
	copy(nid.SzInfo[:], body)

	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
	}

	ok := win32.BoolFromBOOL(win32.Shell_NotifyIcon(win32.NIM_ADD, &nid))
	if !ok {
		return errors.New("failed to create notification")
	}

	if notification.Timeout > 0 {
		// deleting the notification removes it without any animation
		// we only do this if the user wants to make sure its gone after
		// a specific timeout
		// as far as i can tell its okay not to delete the notification
		// on windows 10 its removed automatically
		defer win32.Shell_NotifyIcon(win32.NIM_DELETE, &nid)
	}

	select {
	case <-time.After(notification.Timeout):
		return nil
	case <-ctx.Done():
		return ctx.Err()
	}
}

func createWindow(className string) (win32.HWND, error) {

	var wcex win32.WNDCLASSEX

	// we don't really need a hInstance so we just pass a nullptr
	hInstance := uintptr(0)

	lpszClassName, err := syscall.UTF16PtrFromString(className)
	if err != nil {
		return 0, errors.Wrap(err, "failed to allocate class name")
	}

	windowMessageHandler := func(hwnd win32.HWND, msg uint32, wParam win32.WPARAM, lParam win32.LPARAM) win32.LRESULT {
		switch msg {
		case win32.WM_DESTROY:
			return 0
		default:
			return win32.DefWindowProc(hwnd, msg, wParam, lParam)
		}
	}

	wcex.CbSize = (uint32)(unsafe.Sizeof(wcex)) // winapi needs to know the byte size of this wcex
	wcex.LpfnWndProc = syscall.NewCallback(windowMessageHandler)
	wcex.CbWndExtra = 0
	wcex.HInstance = hInstance
	wcex.LpszClassName = lpszClassName

	_, errCode := win32.RegisterClassEx(&wcex)
	if errCode != 0 {
		return 0, errors.Errorf("win32: RegisterClass failed: Code: %d", errCode)
	}

	// TODO: i don't really know if we need a second ptr
	// maybe it's safe to reuse lpszClassName?
	windowTitle, err := syscall.UTF16PtrFromString(className)
	if err != nil {
		return 0, errors.Wrap(err, "failed to allocate window title")
	}

	windowStyle := win32.WS_OVERLAPPED

	hwnd, errCode := win32.CreateWindowEx(
		0, // no extended window style
		lpszClassName,
		windowTitle,
		windowStyle,
		win32.CW_USEDEFAULT, // default x position
		win32.CW_USEDEFAULT, // default y position
		win32.CW_USEDEFAULT, // default width
		win32.CW_USEDEFAULT, // default height
		0,                   // no parent hwnd
		0,                   // no hmenu
		hInstance,
		nil, // no extra data
	)

	if errCode != 0 {
		return 0, errors.Errorf("win32: CreateWindow failed: Code: %d", errCode)
	}
	return hwnd, nil
}
