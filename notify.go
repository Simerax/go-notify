package notify

import (
	"context"
	"errors"
	"io"
	"time"
)

// Display of Urgency is system dependent.
// Note: will be ignored on windows
type Urgency uint8

const (
	UrgencyLow    Urgency = 0
	UrgencyMedium         = 1
	UrgencyHigh           = 2
)

type Action struct {
	// Id is used to identify the action
	// this is what will be sent over the OnAction channel
	Id string
	// Name is what the user will see in the ui
	Name string
}

// Notification represents a notification to be sent.
// Windows only supports durations "short" and "long"
// freedesktop.notifications supports exact timeouts
type Notification struct {
	// Summary of the notification usually shown as some kind of title
	// Note: length is limited to [128]uint16 on windows
	Summary string

	// Body Text
	// Note: length is limited to [256]uint16 on windows
	Body string

	// Note: will be ignored on windows
	Urgency Urgency

	// Timeout for the notification
	// Note: On Windows this might be ignored. Windows 2000 and XP
	// allow specific timeouts. Other Windows versions do not.
	Timeout time.Duration

	// Absolute path to an icon on the system
	// Note: On Windows this has to be an .ico file
	IconPath string

	// Actions defines a list of possible actions.
	// Provide a channel in the OnAction property to react to actions.
	//
	// Note: Not supported on windows yet.
	// On Unix systems actions may or may not be supported.
	Actions []Action

	// OnAction receives the Id of the triggered action
	// OnAction will be closed as soon as the notification is being closed
	// This property can be left nil
	OnAction chan (string)
}

type Notifier interface {
	Notify(ctx context.Context, n Notification) error
	io.Closer
}

// Error returned by Notify if the notification daemon does not
// support actions. The notification will not be displayed in this case.
var ErrActionsNotSupported = errors.New("actions not supported")

// Error returned by NewNotifier if the platform is not supported
var ErrPlatformNotSupported = errors.New("platform not supported")

func NewNotifier(appName string) (Notifier, error) {
	return newNotifier(appName)
}
