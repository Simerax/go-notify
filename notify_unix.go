//go:build unix

package notify

import (
	"context"
	"errors"

	"github.com/godbus/dbus/v5"
)

type notifier struct {
	appName         string
	conn            *dbus.Conn
	obj             dbus.BusObject
	supportsActions bool
	handlers        chan (map[uint32]chan (string))
}

func (n *notifier) Close() error {
	n.conn.Close()
	n.closeAllHandlers()
	return nil
}

const (
	destination           = "org.freedesktop.Notifications"
	getCapabilitiesMethod = "org.freedesktop.Notifications.GetCapabilities"
	notifyMethod          = "org.freedesktop.Notifications.Notify"
	path                  = "/org/freedesktop/Notifications"

	sNotificationClosed = destination + ".NotificationClosed"
	sActionInvoked      = destination + ".ActionInvoked"
)

func newNotifier(appName string) (Notifier, error) {
	conn, err := dbus.SessionBus()
	if err != nil {
		return nil, err
	}
	obj := conn.Object(destination, dbus.ObjectPath(path))

	n := &notifier{
		appName:  appName,
		conn:     conn,
		obj:      obj,
		handlers: make(chan (map[uint32]chan (string)), 1),
	}
	n.handlers <- map[uint32]chan (string){}

	if err := n.checkActionSupport(); err != nil {
		// if we can't even check capabilites we can assume the server
		// doesn't really work at all
		return nil, err
	}

	sigCh := make(chan (*dbus.Signal))
	conn.Signal(sigCh)

	if err := conn.AddMatchSignal(
		dbus.WithMatchSender(destination),
		dbus.WithMatchObjectPath(path),
	); err != nil {
		return nil, err
	}

	if n.supportsActions {
		go n.handleSignals(sigCh)
	}

	return n, nil
}

func (n *notifier) handleSignals(ch chan (*dbus.Signal)) {
	for {
		select {
		case s, ok := <-ch:
			if !ok {
				// the channel will be automatically closed
				// when n.conn.Close() is called
				return
			}

			switch s.Name {
			case sNotificationClosed:
				n.handleNotificationClosed(s)
			case sActionInvoked:
				n.handleActionInvoked(s)
			default:
				continue
			}
		}
	}
}

func (n *notifier) closeAllHandlers() {
	h := <-n.handlers
	for id, ac := range h {
		close(ac)
		delete(h, id)
	}
	n.handlers <- h
}

func (n *notifier) handleNotificationClosed(s *dbus.Signal) error {
	if len(s.Body) != 2 {
		return errors.New("NotificationClosed: Invalid Arguments count")
	}
	id, ok := s.Body[0].(uint32)
	if !ok {
		return errors.New("NotificationClosed: id is not uint32")
	}

	h := <-n.handlers
	defer func() { n.handlers <- h }()

	ac, ok := h[id]
	if !ok {
		return nil
	}

	close(ac)
	delete(h, id)
	return nil
}

func (n *notifier) handleActionInvoked(s *dbus.Signal) error {

	if len(s.Body) != 2 {
		return errors.New("ActionInvoked: Invalid Arguments count")
	}
	id, ok := s.Body[0].(uint32)
	if !ok {
		return errors.New("ActionInvoked: id is not uint32")
	}
	actionKey, ok := s.Body[1].(string)
	if !ok {
		return errors.New("ActionInvoked: action_key is not string")
	}

	h := <-n.handlers
	defer func() { n.handlers <- h }()

	ac, ok := h[id]
	if !ok {
		return nil
	}

	select {
	case ac <- actionKey:
	default:
		// discard message if client is not able to receive
	}
	return nil
}

func (n *notifier) checkActionSupport() error {
	capabilities := []string{}
	if err := n.obj.Call(getCapabilitiesMethod, 0).Store(&capabilities); err != nil {
		return err
	}
	n.supportsActions = false
	for _, c := range capabilities {
		if c == "actions" {
			n.supportsActions = true
			break
		}
	}
	return nil
}

func (n *notifier) Notify(ctx context.Context, notification Notification) error {

	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
	}

	if !n.supportsActions && (len(notification.Actions) > 0 || notification.OnAction != nil) {
		return ErrActionsNotSupported
	}

	actions := make([]string, 0, len(notification.Actions)*2)
	for _, a := range notification.Actions {
		actions = append(actions, a.Id, a.Name)
	}

	hints := map[string]interface{}{
		"urgency": uint8(notification.Urgency),
	}

	timeout := int32(-1)
	if notification.Timeout > 0 {
		timeout = int32(notification.Timeout.Milliseconds())
	}

	if notification.Urgency == UrgencyHigh && timeout == -1 {
		// display the notification forever if its urgent
		timeout = 0
	}

	call := n.obj.CallWithContext(ctx, notifyMethod, 0,
		n.appName,
		uint32(0), // don't replace any other notification TODO: should Notification have a Replace Property?
		notification.IconPath,
		notification.Summary,
		notification.Body,
		actions,
		hints,
		timeout,
	)

	id := uint32(0)
	if err := call.Store(&id); err != nil {
		return err
	}

	if notification.OnAction != nil {
		h := <-n.handlers
		h[id] = notification.OnAction
		n.handlers <- h
	}
	return nil
}
