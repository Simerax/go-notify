module gitlab.com/Simerax/go-notify

go 1.19

require (
	github.com/godbus/dbus/v5 v5.1.0
	github.com/pkg/errors v0.9.1
	github.com/zzl/go-win32api v1.2.0
)

require golang.org/x/sys v0.0.0-20220330033206-e17cdc41300f // indirect
