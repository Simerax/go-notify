# Go-Notify

Create Desktop Notifications from Go. See examples in the `examples` folder.

Uses D-Bus on unix and Win32 API on windows.
